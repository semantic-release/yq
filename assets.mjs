import SemanticReleaseError from '@semantic-release/error';
import Asset from './asset.mjs';

export default class Assets {
	#value;

	constructor(value) {
		if (!Array.isArray(value)) {
			throw new SemanticReleaseError(
				'`assets` must be an array',
				'EYQCFG',
				`${value} (${typeof value})`,
			);
		}

		this.#value = value;
	}

	[Symbol.iterator] = function * () {
		for (const asset of this.#value) {
			yield new Asset(asset);
		}
	};

	get #array() {
		return Array.from(this);
	}

	map(...args) {
		return this.#array.map(...args);
	}
}
