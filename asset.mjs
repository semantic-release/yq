import SemanticReleaseError from '@semantic-release/error';
import FilePath from './file-path.mjs';
import Expression from './expression.mjs';
import FrontMatter from './front-matter.mjs';

export default class Asset {
	#expression;
	#filepath;
	#frontMatter;

	constructor(value) {
		if (typeof value !== 'object' || Array.isArray(value)) {
			throw new SemanticReleaseError(
				'An asset must be an object',
				'EYQCFG',
				`${value} (${typeof value})`,
			);
		}

		const {expression, filepath, frontMatter} = value;
		this.#expression = new Expression(expression);
		this.#filepath = new FilePath(filepath);
		this.#frontMatter = new FrontMatter(frontMatter);
	}

	get expression() {
		return this.#expression;
	}

	get filepath() {
		return this.#filepath;
	}

	get frontMatter() {
		return this.#frontMatter;
	}

	[Symbol.iterator] = function * () {
		yield this.frontMatter;
		yield this.expression;
		yield this.filepath;
	};

	get #array() {
		return Array.from(this);
	}

	map(...args) {
		return this.#array.map(...args);
	}

	async render(ctx) {
		const promises = this.map(a => a.render(ctx));
		const values = await Promise.all(promises);
		return values.flat();
	}
}
