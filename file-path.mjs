import {access, constants} from 'node:fs/promises';
import path from 'node:path';
import SemanticReleaseError from '@semantic-release/error';
import Argument from './argument.mjs';

export default class FilePath extends Argument {
	constructor(value) {
		if (typeof value !== 'string') {
			throw new SemanticReleaseError(
				'`asset.filepath` must be a string',
				'EYQCFG',
				`${value} (${typeof value})`,
			);
		}

		super(value);
	}

	async #rendered(ctx) {
		try {
			return await super.render(ctx);
		} catch (error) {
			throw new SemanticReleaseError(
				'`asset.filepath` failed to be templated',
				'EYQCFG',
				`${error}`,
			);
		}
	}

	async render(ctx) {
		const location = path.join(ctx.cwd, await this.#rendered(ctx));

		try {
			// eslint-disable-next-line no-bitwise
			await access(location, constants.R_OK | constants.W_OK);
		} catch (error) {
			throw new SemanticReleaseError(
				`Insufficient file access for \`${this.value}\``,
				'EYQCFG',
				`${error}`,
			);
		}

		return location;
	}
}

