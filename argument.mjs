import {template} from 'lodash-es';

export default class Argument {
	#argument;
	#value;

	constructor(...args) {
		switch (args.length) {
			case 1: {
				[this.#value] = args;
				break;
			}

			case 2: {
				[this.#argument, this.#value] = args;
				break;
			}

			default: {
				throw new Error(`Unsupported number of arguments: ${args}`);
			}
		}
	}

	get argument() {
		return this.#argument;
	}

	get value() {
		return this.#value;
	}

	[Symbol.toPrimitive]() {
		return this.value;
	}

	get template() {
		if (this.value === undefined) {
			return () => undefined;
		}

		return template(this.value);
	}

	async render(ctx) {
		const rendered = this.template(ctx);
		if (rendered === undefined) {
			return [];
		}

		if (this.argument !== undefined) {
			return [`${this.argument}`, `${rendered}`];
		}

		return `${rendered}`;
	}
}
