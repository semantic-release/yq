import SemanticReleaseError from '@semantic-release/error';
import Argument from './argument.mjs';

export default class FrontMatter extends Argument {
	static choices = Object.freeze(['extract', 'process']);

	constructor(value) {
		if (value === undefined) {
			// Pass
		} else if (typeof value !== 'string') {
			throw new SemanticReleaseError(
				'`asset.frontMatter` must be a string',
				'EYQCFG',
				`${value} (${typeof value})`,
			);
		} else if (!FrontMatter.choices.includes(value)) {
			throw new SemanticReleaseError(
				`\`asset.frontMatter\` must be one of ${FrontMatter.choices.map(s => `\`${s}\``)}`,
				'EYQCFG',
			);
		}

		super('--front-matter', value);
	}
}

