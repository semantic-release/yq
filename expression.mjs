import SemanticReleaseError from '@semantic-release/error';
import Argument from './argument.mjs';

export default class Expression extends Argument {
	constructor(value) {
		if (typeof value !== 'string') {
			throw new SemanticReleaseError(
				'`asset.expression` must be a string',
				'EYQCFG',
				`${value} (${typeof value})`,
			);
		}

		super('--expression', value);
	}

	async render(ctx) {
		try {
			return await super.render(ctx);
		} catch (error) {
			throw new SemanticReleaseError(
				'`asset.expression` failed to be templated',
				'EYQCFG',
				`${error}`,
			);
		}
	}
}

