import {stat, readdir, access, constants} from 'node:fs/promises';
import {fileURLToPath} from 'node:url';
import os from 'node:os';
import path from 'node:path';
import which from 'which';

const BIN = (() => {
	const file = fileURLToPath(import.meta.url);
	const dirname = path.dirname(file);
	const bin = path.join(dirname, 'bin');
	return bin;
})();

const ARCH = {
	arm: 'arm',
	arm64: 'arm64',
	ppc64: 'ppc64',
	x64: 'amd64',
	s390: 's390',
	s390x: 's390x',
	mips: 'mips',
	riscv64: 'riscv64',
}[os.arch()];

const PLATFORM = {
	darwin: 'darwin',
	linux: 'linux',
	freebsd: 'freebsd',
	openbsd: 'openbsd',
	netbsd: 'netbsd',
	win32: 'windows',
}[os.platform()];

export async function bins(bin = BIN) {
	const possibles = await readdir(bin);
	const stats = await Promise.all(possibles.map(filepath => stat(path.join(bin, filepath))));
	return possibles.filter((_, i) => {
		const status = stats[i];
		// eslint-disable-next-line no-bitwise
		return status.isFile() && (status.mode & constants.S_IXUSR);
	});
}

export default async function yq(bin = BIN, platform = PLATFORM, arch = ARCH) {
	const extension = platform === 'windows' ? /* c8 ignore next */ '.exe' : '';
	const basename = `yq_${platform}_${arch}${extension}`;
	const filepath = path.join(bin, basename);
	try {
		await access(filepath, constants.X_OK);
		return filepath;
	/* c8 ignore next 3 */
	} catch {
		return which('yq').catch(() => undefined);
	}
}
