# yq

A `semantic-release` plugin to update JSON, YAML, TOML and more files with
[`yq`].

| Step             | Description                                                         |
| ---------------- | ------------------------------------------------------------------- |
| verifyConditions | Verify that each file is writable and the `yq` expression is valid. |
| prepare          | Updates each file in-place with the `yq` expression provided.       |

## Getting Started

```sh
npm config --location project set registry https://gitlab.arm.com/api/v4/groups/semantic-release/-/packages/npm/
npm install --save-dev @semantic-release/yq
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "some/file.yaml"
        expression: |-
          .version = "${nextRelease.version}"
```

## Usage

### Version Bumps

The most common usage is to bump a version in a JSON, YAML or TOML file:

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "some/file.yaml"
        expression: |-
          .version = "${nextRelease.version}"
```

## Configuration

At least _one_ file must be provided to the plugin configuration.

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "./some/file.yaml"
        expression: ".a = .b"
```

### `assets[*].filepath`

A relative filepath modify in-place.

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "./some/some.yaml"
        expression: ".a = .b"
```

### `assets[*].expression`

The `yq` [expression][quick-usage-guide] to execute on the provided file.

Can use [lodash template] `semantic-release` [substitutions].

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "./some/file.yaml"
        expression: |-
          .version = "${nextRelease.version}"
```

### `assets[*].frontMatter`

Determines if the `expression` should operate on the YAML [front
matter][front-matter].

```yaml
plugins:
  - path: "@semantic-release/yq"
    assets:
      - filepath: "./some/file.yaml"
        frontMatter: "process"
        expression: |-
          .version = "${nextRelease.version}"
```

### `yq`

The NPM package contains a certain set of hermetic `yq` binaries.

To keep the package size small, not _all_ `yq` binaries are provided.

Options are:

- Provide a `yq` binaries on the `$PATH`
- Set the `yq` configuration variable
  ```yaml
  plugins:
    - path: "@semantic-release/yq"
      yq: "/usr/bin/yq"
  ```
- Request for more `yq` binaries to be included in the package

Binaries are included in the package with a `prepublish.mjs` download script.

[yq]: https://github.com/mikefarah/yq
[quick-usage-guide]:
  https://github.com/mikefarah/yq?tab=readme-ov-file#quick-usage-guide
[lodash template]: https://docs-lodash.com/v4/template/
[substitutions]:
  https://semantic-release.gitbook.io/semantic-release/developer-guide/js-api#nextrelease
[front-matter]: https://mikefarah.gitbook.io/yq/usage/front-matter
