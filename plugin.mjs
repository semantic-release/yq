import os from 'node:os';
import SemanticReleaseError from '@semantic-release/error';
import debug from 'debug';
import {$} from 'execa';
import find, {bins as available} from './yq.mjs';
import Assets from './assets.mjs';

debug('semantic-release:yq');

export async function verifyConditions(pluginConfig, context) {
	const {assets: value, yq = await find()} = pluginConfig;
	const {logger, ...rest} = context;

	/* c8 ignore next */
	if (yq === undefined) {
		/* c8 ignore next 6 */
		const bins = await available().catch(() => []);
		throw new SemanticReleaseError(
			'No `yq` binary available',
			'EYQCFG',
			`No binary available for \`${os.arch()}\`/\`${os.platform}\`. Available hermetic binaries are: ${bins.length === 0 ? 'none' : bins.map(b => `\`${b}\``).join(', ')}. Either provide one on \`$PATH\`, set the \`yq\` configuration or request for more hermetic binaries to be packaged in the plugin.`,
		);
	} else if (typeof yq !== 'string') {
		throw new SemanticReleaseError(
			'`yq` must be a string',
			'EYQCFG',
			`${yq} (${typeof yq})`,
		);
	}

	const assets = new Assets(value);

	const ctx = {
		nextRelease: {
			type: 'patch',
			version: '0.0.0',
			gitHead: '0123456789abcedf0123456789abcdef12345678',
			gitTag: 'v0.0.0',
			notes: 'placeholder',
		},
		...rest,
	};

	const checks = assets.map(async asset => {
		try {
			await $`${yq} ${await asset.render(ctx)}`;
		} catch (error) {
			if (error instanceof SemanticReleaseError) {
				throw error;
			}

			throw new SemanticReleaseError(
				'Running `yq` failed',
				'EYQ',
				`Attempted to run:\n\n\`\`\`${error.command}\`\`\`\n\nIt failed with the following message:\n\n\`\`\`${error.stderr}\`\`\``,
			);
		}

		logger.success('Validated `%s`', await asset.filepath.render(ctx));
	});

	await Promise.all(checks);
	logger.success('Validated `assets` configuration');
}

export async function prepare(pluginConfig, context) {
	const {assets: value, yq = await find()} = pluginConfig;
	const {logger, ...ctx} = context;

	const assets = new Assets(value);

	const updates = assets.map(async asset => {
		await $`${yq} --inplace ${await asset.render(ctx)}`;
		logger.success('Wrote `%s`', await asset.filepath.render(ctx));
	});

	await Promise.all(updates);
}
