import test from 'ava';
import Asset from '../asset.mjs';

test('`Asset` cannot be constructed with a string', t => {
	t.throws(() => new Asset('invalid'));
});

test('`Asset` cannot be constructed with an array', t => {
	t.throws(() => new Asset([]));
});
