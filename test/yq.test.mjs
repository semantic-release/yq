import test from 'ava';
import {bins} from '../yq.mjs';

test('`yq.bins` can find some binaries', async t => {
	const binaries = await bins();
	t.not(binaries.length, 0);
});
