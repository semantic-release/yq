import test from 'ava';
import FrontMatter from '../front-matter.mjs';

test('`FrontMatter` cannot be constructed with an object', t => {
	t.throws(() => new FrontMatter({}));
});

test('`FrontMatter` cannot be constructed with an invalid choice', t => {
	t.throws(() => new FrontMatter('what'));
});
