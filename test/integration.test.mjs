import path from 'node:path';
import {readFile, writeFile, mkdir, unlink} from 'node:fs/promises';
import test from 'ava';
import {restore, stub} from 'sinon';
import clearModule from 'clear-module';
import {WritableStreamBuffer} from 'stream-buffers';
import {temporaryDirectory} from 'tempy';

test.beforeEach(async t => {
	clearModule('../plugin.mjs');
	t.context.m = await import('../plugin.mjs');
	t.context.log = stub();
	t.context.tmp = temporaryDirectory();
	const dir = temporaryDirectory();

	t.context.filepath = path.join(dir, 'nested', 'file.yaml');
	await mkdir(path.dirname(t.context.filepath));
	await writeFile(t.context.filepath, 'version: "0.0.0"');

	t.context.cfg = {
		assets: [
			{
				filepath: path.relative(dir, t.context.filepath),
				// eslint-disable-next-line no-template-curly-in-string
				expression: '.version = "${nextRelease.version}"',
			},
		],
	};
	t.context.ctx = {
		cwd: dir,
		env: {},
		options: {},
		stdout: new WritableStreamBuffer(),
		stderr: new WritableStreamBuffer(),
	};
	t.context.ctx.logger = {
		log: t.context.log,
		info: t.context.log,
		success: t.context.log,
		warn: t.context.log,
	};
});

test.afterEach(() => {
	restore();
});

const failure = test.macro(async (t, before, after) => {
	if (before) {
		await before(t);
	}

	const [, code, pattern] = /`(.+)` error when (.+)/.exec(t.title);
	const regex = new RegExp(`${pattern[0].toUpperCase()}${pattern.slice(1)}`);

	const error = await t.throwsAsync(
		t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
	);

	t.is(error.name, 'SemanticReleaseError', `${error}`);
	t.is(error.code, code, `${error}`);
	t.regex(error.message, regex, `${error}`);

	if (after) {
		await after(t);
	}
});

const success = test.macro(async (t, before, after) => {
	if (before) {
		await before(t);
	}

	await t.notThrowsAsync(
		t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
	);
	t.context.ctx.nextRelease = {version: '1.0.0'};
	await t.notThrowsAsync(t.context.m.prepare(t.context.cfg, t.context.ctx));
	await t.notThrowsAsync(
		t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
	);

	if (after) {
		await after(t);
	}
});

test('Throws `EYQCFG` error when insufficient file access for `.+`', failure, async t => {
	await unlink(t.context.filepath);
});

test('Throws `EYQCFG` error when `yq` must be a string', failure, async t => {
	t.context.cfg.yq = 1;
});

test('Throws `EYQCFG` error when `asset.filepath` must be a string', failure, async t => {
	t.context.cfg.assets[0].filepath = 1;
});

test('Throws `EYQCFG` error when `asset.expression` must be a string', failure, async t => {
	t.context.cfg.assets[0].expression = 1;
});

test('Throws `EYQCFG` error when `asset.expression` failed to be templated', failure, async t => {
	// eslint-disable-next-line no-template-curly-in-string
	t.context.cfg.assets[0].expression = '.v = "${nope}"';
});

test('Throws `EYQ` error when running `yq` failed', failure, async t => {
	t.context.cfg.assets[0].expression = '.v = nope';
});

test('Verify and prepare assets', success, undefined, async t => {
	const data = await readFile(t.context.filepath, {encoding: 'utf8'});
	t.is(data, 'version: "1.0.0"\n');
});

test('Verify and prepare assets with front matter', success, async t => {
	t.context.cfg.assets[0].frontMatter = 'process';

	const data = await readFile(t.context.filepath, {encoding: 'utf8'});
	await writeFile(t.context.filepath, `version: "0.0.0"\n---\n${data}`);
}, async t => {
	const data = await readFile(t.context.filepath, {encoding: 'utf8'});
	t.is(data, 'version: "1.0.0"\n---\nversion: "0.0.0"');
});
