import test from 'ava';
import Argument from '../argument.mjs';

test('`Argument` cannot be constructed with zero arguments', t => {
	t.throws(() => new Argument());
});

test('`Argument` can be constructed with one argument', t => {
	const argument = new Argument('one');
	t.is(argument.argument, undefined);
	t.is(argument.value, 'one');
});

test('`Argument` can be constructed with two arguments', t => {
	const argument = new Argument('one', 'two');
	t.is(argument.argument, 'one');
	t.is(argument.value, 'two');
});

test('`Argument` cannot be constructed with three arguments', t => {
	t.throws(() => new Argument('one', 'two', 'three'));
});

test('`Argument` can be converted to a primitive', t => {
	const argument = new Argument('one');
	t.is(`${argument}`, 'one');
});

test('`Argument` can be rendered with one argument', async t => {
	// eslint-disable-next-line no-template-curly-in-string
	const argument = new Argument('${one}');
	const rendered = await argument.render({one: 1});
	t.is(rendered, '1');
});

test('`Argument` can be rendered with two arguments', async t => {
	// eslint-disable-next-line no-template-curly-in-string
	const argument = new Argument('--yes', '${one}');
	const rendered = await argument.render({one: 1});
	t.deepEqual(rendered, ['--yes', '1']);
});
