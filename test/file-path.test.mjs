import test from 'ava';
import FilePath from '../file-path.mjs';

test('`FilePath` cannot be constructed with a object', t => {
	t.throws(() => new FilePath({}));
});

test('`FilePath` throws with an invalid templated filepath', async t => {
	// eslint-disable-next-line no-template-curly-in-string
	const argument = new FilePath('${one}');
	await t.throwsAsync(async () => argument.render({}));
});

test('`FilePath` throws with an invalid filepath', async t => {
	const argument = new FilePath('this-does-not-exist');
	await t.throwsAsync(async () => argument.render({}));
});
