#! /usr/bin/env node

/*
 * This script downloads pre-built `yq` binaries.
 * It is controllable with three NPM configuration variables:
 *   - `url`: The URL base to a `yq` download page, can include `${version}`
 *   - `version`: A `yq` version to template the `url` with
 *   - `filter`: A regular expression to select the `yq` binaries to download
 * A local `npm` install will download the current host binary.
 * The configuration values can be changed in `package.json` or via `npm config`:
 *
 *     npm config set @semantic-release/yq:version 4.41.0
 *
 * Binaries can be filtered to reduce the total size of the uploaded NPM package.
 * Check the size of the package with `npm pack`.
 */

import os from 'node:os';
import path from 'node:path';
import {createReadStream} from 'node:fs';
import {Readable} from 'node:stream';
import {stat, unlink, rmdir, mkdir, mkdtemp, open, rename} from 'node:fs/promises';
import {fileURLToPath} from 'node:url';
import process from 'node:process';
import {createHash} from 'node:crypto';

// Figure out the base URL from the package configuration
const version = process.env.npm_package_config_version;
const ctx = {version};
const rendered = process.env.npm_package_config_url.replace(/\${(\w+)}/, (match, key) => {
	const value = ctx[key];
	return value === undefined ? match : value;
});
const base = `${rendered}/`;

// Some helper functions for `fetch`
const ok = response => {
	if (!response.ok) {
		throw new Error(`Failed to retrieve ${response.url}: ${response.status}`);
	}

	return response;
};

const get = (...urls) => {
	if (urls.length > 1) {
		return Promise.all(urls.map(u => get(u)));
	}

	return fetch(new URL(urls[0], base)).then(ok);
};

const lines = text => text.trimEnd().split('\n');

// Grab the manifest of filenames and their checksums
const manifest = await (async () => {
	const [checksums, order] = await get('checksums', 'checksums_hashes_order');
	const algorithms = await order.text().then(lines);
	const arrays = await checksums.text().then(lines).then(a => a.map(l => l.split('  ')));
	const entries = arrays.map(([name, ...hashes]) => {
		const kv = hashes.map((h, i) => [algorithms[i].toLowerCase(), h]);
		return [name, Object.fromEntries(kv)];
	});
	const manifest = Object.fromEntries(entries);
	return manifest;
})();

// Filter to download just for host if doing local install
const arch = {
	arm: 'arm',
	arm64: 'arm64',
	ppc64: 'ppc64',
	x64: 'amd64',
	s390: 's390',
	s390x: 's390x',
	mips: 'mips',
	riscv64: 'riscv64',
}[os.arch()];
const platform = {
	darwin: 'darwin',
	linux: 'linux',
	freebsd: 'freebsd',
	openbsd: 'openbsd',
	netbsd: 'netbsd',
	win32: 'windows',
}[os.platform()];
const extension = platform === 'windows' ? '.exe' : '';
const suffix = `_${platform}_${arch}${extension}`;
const regex = new RegExp(process.env.npm_package_config_filter);
const predicate = name => regex.test(name);
const host = name => name.endsWith(suffix);
const filter = process.env.npm_command === 'install' ? host : predicate;

// Helpful progress stream
class ProgressStream extends TransformStream {
	constructor(reporter, total, ...args) {
		let received = 0;
		super({
			start() {},
			async transform(chunk, controller) {
				received += chunk.length;
				reporter(received, total, args);
				controller.enqueue(chunk);
			},
		});
	}
}

// Helper classes to hash the downloaded file
class HashError extends Error {
	constructor(message, actual, expected) {
		super(message);
		this.name = 'HashError';
		this.actual = actual;
		this.expected = expected;
	}
}

class HashStream extends TransformStream {
	constructor(expected, algorithm = 'sha-256') {
		const hash = createHash(algorithm);
		super({
			start() {},
			transform(chunk, controller) {
				hash.update(chunk);
				controller.enqueue(chunk);
			},
			flush() {
				const actual = hash.digest('hex');
				if (actual !== expected) {
					throw new HashError('Digest did not match', actual, expected);
				}
			},
		});
	}
}

// A safe filewriter
class FileWritableStream extends WritableStream {
	constructor(filepath, mode = 0o644) {
		super({
			async start() {
				this.filepath = path.join(await mkdtemp('fetch'), path.basename(filepath));
				this.handle = await open(this.filepath, 'w');
			},
			async write(chunk) {
				await this.handle.write(chunk);
			},
			async close() {
				await this.handle.chmod(mode);
				await this.handle.close(this.handle);
				await mkdir(path.dirname(filepath), {recursive: true});
				await rename(this.filepath, filepath);
				await rmdir(path.dirname(this.filepath));
			},
			async abort() {
				await this.handle.close(this.handle);
				await unlink(this.filepath);
				await rmdir(path.dirname(this.filepath));
			},
		});
	}
}

// Unlink file on error
class UnlinkStream extends WritableStream {
	constructor(filepath) {
		super({
			start() {},
			async abort() {
				try {
					await unlink(filepath);
				} catch (error) {
					if (error.code !== 'ENOENT') {
						throw error;
					}
				}
			},
		});
	}
}

// Determine the binary output location
const file = fileURLToPath(import.meta.url);
const dirname = path.dirname(file);
const bin = path.join(dirname, 'bin');

// Progress reporter
class Reporter extends Function {
	constructor(message) {
		super('...args', 'return this.report(...args)');
		this.message = message;
		this.map = new Map();
		this.count = 0;
		return this.bind(this); // eslint-disable-line no-constructor-return
	}

	report(received, total, key) {
		this.map.set(key, received / total);
		const size = this.map.size;
		let progress = 0;
		let complete = 0;
		for (const value of this.map.values()) {
			progress += value;
			if (value >= 1) {
				complete += 1;
			}
		}

		progress /= size;

		if (process.stderr.isTTY) {
			process.stderr.write('\r\u001B[K');
		}

		process.stderr.write(`${this.message}: ${complete} / ${size} (${(progress * 100).toPrecision(4)}%)`);
		if (complete === size || !process.stderr.isTTY) {
			process.stderr.write('\n');
		}
	}
}

// Download each file
const checked = new Reporter('Checking `yq` binaries');
const downloaded = new Reporter('Downloading `yq` binaries');
const download = async (name, checksums, algorithm = 'sha-256') => {
	const filepath = path.join(bin, name);
	try {
		const status = await stat(filepath);
		await Readable.toWeb(createReadStream(filepath))
			.pipeThrough(new ProgressStream(checked, status.size, filepath))
			.pipeThrough(new HashStream(checksums[algorithm], algorithm))
			.pipeTo(new UnlinkStream(filepath));
		return;
	} catch (error) {
		if (error instanceof HashError) {
			// Fallthrough
		} else if (error.code !== 'ENOENT') {
			throw error;
		}
	}

	const response = await get(name);
	const url = new URL(response.url);
	const total = response.headers.get('Content-Length');
	const stream = response.body
		.pipeThrough(new ProgressStream(downloaded, total, url))
		.pipeThrough(new HashStream(checksums[algorithm], algorithm))
		.pipeTo(new FileWritableStream(filepath, 0o755));
	await stream;
};

const downloads = Object.entries(manifest)
	.filter(([name]) => filter(name))
	.map(([name, checksums]) => download(name, checksums));
await Promise.all(downloads);
